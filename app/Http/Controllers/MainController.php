<?php

namespace App\Http\Controllers;

use App\Services\VK\EventService;

class MainController extends Controller
{
    /**
     * Обработать приходящий запрос от Внешнего сервиса
     * @return string
     */
    public function handle(): string
    {
        $event = json_decode(file_get_contents('php://input'), true);

        return EventService::handleEvent($event);
    }
}
