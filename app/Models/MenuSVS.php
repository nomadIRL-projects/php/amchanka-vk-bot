<?php

namespace App\Models;

class MenuSVS
{
    const SHAVA_COVER = [
        'standart' => 'Обычный',
        'cheese' => 'Сырный'
    ];

    const SHAVA_SAUCE = [
        'classic' => 'Классический',
        'ketchup' => 'Томатный',
        'onion' => 'Чесночный'
    ];

    const SHAVA_TYPE = [
        'chicken' => [
            '_name' => 'Шаурма с курицей',
            'small' => [
                '_name' => 'Мини',
                'price' => 99
            ],
            'medium' => [
                '_name' => 'Стандарт',
                'price' => 139
            ],
            'big' => [
                '_name' => 'Макси',
                'price' => 189
            ],
        ],

        'pork' => [
            '_name' => 'Шаурма со свининой',
            'small' => [
                '_name' => 'Мини',
                'price' => 119
            ],
            'medium' => [
                '_name' => 'Стандарт',
                'price' => 159
            ],
            'big' => [
                '_name' => 'Макси',
                'price' => 209
            ],
        ],
        'beaf' => [
            '_name' => 'Шаурма с телятиной',
            'small' => [
                '_name' => 'Мини',
                'price' => 159
            ],
            'medium' => [
                '_name' => 'Стандарт',
                'price' => 209
            ],
            'big' => [
                '_name' => 'Макси',
                'price' => 259
            ],
        ],
        'salmon' => [
            '_name' => 'Шаурма с лососем',
            'small' => [
                '_name' => 'Мини',
                'price' => 149
            ],
            'medium' => [
                '_name' => 'Стандарт',
                'price' => 199
            ],
            'big' => [
                '_name' => 'Макси',
                'price' => 249
            ],
        ],
        'vegan' => [
            '_name' => 'Шаурма овощная',
            'small' => [
                '_name' => 'Мини',
                'price' => 79
            ],
            'medium' => [
                '_name' => 'Стандарт',
                'price' => 109
            ],
            'big' => [
                '_name' => 'Макси',
                'price' => 129
            ],
        ],
        'east' => [
            '_name' => 'Восточная шаурма',
            'small' => [
                '_name' => 'Мини',
                'price' => 129
            ],
            'medium' => [
                '_name' => 'Стандарт',
                'price' => 169
            ],
            'big' => [
                '_name' => 'Макси',
                'price' => 209
            ],
        ],
        'caesar' => [
            '_name' => 'Цезарь ролл',
            'price' => 149
        ],
    ];
}
