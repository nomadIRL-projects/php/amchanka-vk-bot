<?php

namespace App\Models\External\VK;

/**
 * Class Methods
 * @package App\Models\External\VK
 */
class Methods extends Auth
{
    /** @var string Метод получения проверочного кода */
    const GET_CONFIRMATION = 'groups.getCallbackConfirmationCode';

    /** @var string Метод отправки личного сообщения */
    const MESSAGE_SEND = 'messages.send';
}
