<?php

namespace App\Models\External\VK;

/**
 * Class Events
 * @package App\Models\External\VK
 */
class Events
{
    const CONFIRMATION = 'confirmation';
    const MESSAGE_NEW = 'message_new';
    const MESSAGE_EVENT = 'message_event';
}
