<?php

namespace App\Models\External\VK;

/**
 * Class Auth
 * @package App\Models\External\VK
 * @property string $v
 * @property int $group_id
 * @property string $access_token
 */
class Auth
{
    const PERSONAL_GROUP_KEY = '6badbe15716f24b2856aa35b01ca4e1a969d6549d22a834bb75f7add36841adc097c0ab3c93dfe0a7a521';
    const PERSONAL_GROUP_ID = 201093165;

    const API_SERVER = 'https://api.vk.com/method/';
    const API_VERSION = '5.126';

    /**
     * Auth constructor.
     * @param array|null $params
     */
    public function __construct(?array $params)
    {
        $this->v = self::API_VERSION;
        $this->group_id = self::PERSONAL_GROUP_ID;
        $this->access_token = self::PERSONAL_GROUP_KEY;

        if ($params) {
            foreach ($params as $param => $value) {
                $this->$param = $value;
            }
        }
    }
}
