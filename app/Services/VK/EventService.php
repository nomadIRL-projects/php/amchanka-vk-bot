<?php

namespace App\Services\VK;

use App\Http\Controllers\BranchController;
use App\Models\External\VK\Auth;
use App\Models\External\VK\Events;
use App\Models\External\VK\Methods;
use Exception;
use Illuminate\Support\Facades\Log;

class EventService
{
    /**
     * Обрабатывает событие
     * @param $event
     * @return string
     */
    public static function handleEvent($event): string
    {
        Log::info('EVENT IS LIKE: ' . json_encode($event));
        switch ($event['type']) {
            case Events::CONFIRMATION:
                return '6d5b0025';
            case Events::MESSAGE_NEW:
                $message = $event['object']['message'];
                Log::info('MESSAGE IS LIKE: ' . json_encode($message));

                $peer_id = $message['peer_id'] ?? $message['user_id'];
                try {
                    $command = json_decode($message['payload'])->button;
                } catch (Exception $exception) {
                    $command = '';
                }

                $botResponse = BranchController::handleBranch($command);

                self::sendMessage(
                    $peer_id,
                    $botResponse
                );
                return 'ok';
            default:
                return 'Unsupported event';
        }
    }

    /**
     * Подтвердить сервер в первый раз
     * @return string
     */
    private static function confirm(): string
    {
        return '90ebaa47';
    }

    /**
     * Отправить сообщение пользователю
     * @param int|null $peerId
     * @param string|null $message
     * @param bool $needKeyboard
     */
    private static function sendMessage(?int $peerId, ?string $message, bool $needKeyboard = false): void
    {
        self::performRequest(Methods::MESSAGE_SEND, [
            'peer_id' => $peerId,
            'message' => $message,
            'random_id' => rand(1, 100),
            'keyboard' => json_encode([
                'one_time' => false,
                'buttons' => [
                    [
                        # Первая кнопка
                        [
                            'color' => 'secondary',
                            'action' => [
                                'type' => 'text',
                                'label' => 'Жопа',
                                'payload' => json_encode([
                                    'button' => 'jopa'
                                ]),
                            ],
                        ],
                        # Вторая кнопка
                        [
                            'color' => 'secondary',
                            'action' => [
                                'type' => 'text',
                                'label' => 'Сыр',
                                'payload' => json_encode([
                                    'button' => 'sir'
                                ]),
                            ],
                        ]
                    ],
                ]
            ])
        ]);
    }

    /**
     * Отправляет запрос в VK и получает ответ
     * @param string $method
     * @param array|null $params
     * @return mixed
     */
    private static function performRequest(string $method, ?array $params): string
    {
        $params = get_object_vars(new Auth($params));
        $query = http_build_query($params);
        $url = Methods::API_SERVER . $method . '?' . $query;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        if (!isset($response->response)) {
            Log::error('Error occurred: ' . json_encode($response));
            return 'Error occurred, see logs';
        } else {
            return $response->response;
        }
    }
}
